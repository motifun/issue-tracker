<div align="center">
    <a href="https://www.motifun.de/" alt="MotiFun.de">
        <img src="https://static.motifun.de/icons/icon-192x192.png">
    </a>
</div>

<div align="center">
    <a href="https://gitlab.com/motifun/issue-tracker/issues?state=opened">
        <img src="https://img.shields.io/badge/dynamic/json.svg?label=Offene Issues&url=https://gitlab.com/api/v4/groups/motifun/issues_statistics&query=$.statistics.counts.opened&colorB=yellow" alt="Offene Issues">
    </a>
    <a href="https://gitlab.com/motifun/issue-tracker/issues?state=closed">
        <img src="https://img.shields.io/badge/dynamic/json.svg?label=Geschlossene Issues&url=https://gitlab.com/api/v4/groups/motifun/issues_statistics&query=$.statistics.counts.closed&colorB=brightgreen" alt="Geschlossene Issues">
    </a>
    <a href="https://www.motifun.de">
        <img src="https://img.shields.io/website.svg?down_color=red&down_message=down&label=Website%20status&up_color=green&up_message=up&url=https://motifun.de" alt="Website Status">
    </a>
    <a href="https://twitter.com/motifun">
        <img src="https://img.shields.io/twitter/follow/motifun.svg?label=MotiFun&style=social" alt="Twitter">
    </a>
</div>


---


# MotiFun.de Issue-Tracker

Hallo du da draußen!

Cool, dass du den Weg zu diesem Repository gefunden hast. Es dient dazu MotiFun noch besser zu machen und funktioniert, indem du hier eine so genannte Issue (= "Meldung") eröffnest. Dafür haben wir zwei Templates bereitgestellt. Zum einen eins, um Fehler zu melden und zum anderen eins um uns Entwicklungs- oder Designideen mitzuteilen.

# Wie erstelle ich eine Issue
 1. [Erstelle dir einen GitLab Account](https://gitlab.com/users/sign_in) oder [melde dich mit deinem bestehenden an](https://gitlab.com/users/sign_in)
 2. Erstelle eine neue Issue, indem du entweder [hier](https://gitlab.com/motifun/issue-tracker/issues/new) klickst oder links im Menu den Eintrag Issues auswählst, dann auf "List" klickst und dann den Button "New Issue" drückst.
 3. Wähle eine unserer Vorlagen
 4. Lege einen Titel fest
 5. Versuche alle Felder auszufüllen und möglichst genau zu beschreiben
 6. Öffne das Preview-Tab, um zu prüfen, ob die Formatierung korrekt ist

# Sicherheit
Falls du eine sicherheitsrelevante Meldung hast, dann sende uns bitte eine E-Mail an [issues@motifun.de](mailto:issues@motifun.de). Selbstverständlich kannst du uns auch zu anderen Problemen eine E-Mail schreiben, statt hier eine Issue zu eröffnen.


---


[🔗 MotiFun.de](https://motifun.de)  
[🐦 @MotiFun auf Twitter](https://twitter.com/motifun)