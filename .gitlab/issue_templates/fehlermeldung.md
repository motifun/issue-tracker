---
name: Fehlermeldung
about: Erstelle eine Meldung zu einem Fehler
title: ''
labels: bug
assignees: ''

---

**Beschreibe den Fehler**  
Eine klare und knappe Beschreibung, um welchen Fehler es sich handelt.

**Reproduktion**  
Schritte um den Fehler zu reproduzieren:
1. Öffne die Seite 'https://www.motifun.de/Seite-mit-Fehler'
2. Klicke auf '....'

**Erwartetes Verhalten**  
Eine Beschreibung des Fehlers oder des fehlerhaften Verhaltens.

**Screenshots**  
Wenn nötig, Screenshot anfügen, welche das Problem veranschaulichen.
(*Screenshots per Drag & Drop hier einfügen*)

**Desktop (bitte die Informationen vervollständigen):**  
 - Betriebssystem: [z.B. Windows 10]
 - Browser: [z.B. Firefox, Safari, Chrome]
 - Browser-Version: [z.B. 22]

**Smartphone (bitte die Informationen vervollständigen):**  
 - Gerät: [z.B. iPhone11]
 - Betriebssystem: [z.B. iOS13]
 - Browser: [z.B. Firefox, Safari]
 - Browser-Version: [z.B. 69]

**Zusätzliche Informationen**  
Weitere Informationen über das Problem.